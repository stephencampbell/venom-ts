import IPost from '~shared/types/post';

export interface IState {
  authToken: string | null
  user:      any
  users:     any
  posts:     IPost[]
}
