import Vue                    from 'vue';
import Vuex, { StoreOptions } from 'vuex';
import createPersistedState   from 'vuex-persistedstate';

import { IState }             from './types';
import mutations              from './mutations';
import getters                from './getters';
import actions                from './actions';

Vue.use(Vuex);

export const state: IState = {
  authToken: null,
  user:      null,
  users:     [],
  posts:     []
};

const storeData: StoreOptions<IState> = {
  state,
  getters,
  mutations,
  actions,
  plugins: [
    createPersistedState()
  ]
};

export default new Vuex.Store<IState>(storeData);
