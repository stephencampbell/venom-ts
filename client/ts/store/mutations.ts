import { MutationTree } from 'vuex';
import { IState }       from './types';
import IPost            from '~shared/types/post';
import IUser            from '~shared/types/user';

const mutations: MutationTree<IState> = {
  setAuthToken (state: IState, token: string) {
    state.authToken = token;
  },
  setUser (state: IState, user: IUser) {
    state.user = user;
  },
  setUsers (state: IState, users: IUser[]) {
    state.users = users;
  },
  setPosts (state: IState, posts: IPost[]) {
    state.posts = posts;
  },
  addPost (state: IState, post: IPost) {
    state.posts.push(post);
  },
  clearAuthData (state: IState) {
    state.authToken = null;
    state.user      = null;
  }
};

export default mutations;
