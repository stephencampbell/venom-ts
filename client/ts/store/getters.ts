import { GetterTree } from 'vuex';
import { IState }     from './types';

const getters: GetterTree<IState, any> = {
  authToken: (state: IState): IState['authToken'] => state.authToken,
  user:      (state: IState): IState['user'] => state.user,
  users:     (state: IState): IState['users'] => state.users,
  posts:     (state: IState): IState['posts'] => state.posts
};

export default getters;
