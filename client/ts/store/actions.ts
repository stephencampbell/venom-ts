import axios          from 'axios';
import { ActionTree } from 'vuex';
import { IState }     from './types';
import IPost          from '~shared/types/post';

const actions: ActionTree<IState, any> = {
  loadUsers ({ getters, commit }: any) {
    return new Promise((resolve, reject) => {
      axios
          .get('/api/users/all', {
            headers: {
              Authorization: getters.authToken
            }
          })
          .then(res => {
            commit('setUsers', res.data);
            resolve(res.data);
          })
          .catch(err => {
            if(err.response) {
              if(err.response.status === 401) {
                commit('clearAuthData');
              }
            }
            reject(err);
          });
    })
  },
  newPost ({ getters, commit }: any, { title, content }: IPost) {
    return new Promise((resolve, reject) => {
      axios
          .post('/api/posts/add', {
            title,
            content
          }, {
            headers: {
              Authorization: getters.authToken
            }
          })
          .then(res => {
            commit('addPost', res.data);
            resolve(res.data);
          })
          .catch(err => {
            if(err.response) {
              if(err.response.status === 401) {
                commit('clearAuthData');
              }
            }
            reject(err);
          });
    })
  },
  loadPosts ({ commit }: any) {
    return new Promise((resolve, reject) => {
      axios
          .get('/api/posts/recent')
          .then(res => {
            commit('setPosts', res.data);
            resolve(res.data);
          })
          .catch(err => {
            if(err.response) {
              if(err.response.status === 401) {
                commit('clearAuthData');
              }
            }
            reject(err);
          });
    })
  }
};

export default actions;
