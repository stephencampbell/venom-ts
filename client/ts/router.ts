import Vue       from 'vue';
import VueRouter from 'vue-router';

import Home     from './components/pages/Home.vue';
import Login    from './components/pages/Login.vue';
import Register from './components/pages/Register.vue';
import Profile  from './components/pages/Profile.vue';
import Posts    from './components/pages/Posts.vue';
import Chat     from './components/pages/Chat.vue';

Vue.use(VueRouter);

const routes = [
  { path: '/', component: Home },
  { path: '/login', component: Login },
  { path: '/register', component: Register },
  { path: '/profile/:slug', component: Profile },
  { path: '/posts/', component: Posts },
  { path: '/chat/', component: Chat }
];

export default new VueRouter({
  mode: 'history',
  routes
});
