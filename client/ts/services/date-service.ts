import moment from 'moment';

export function formatDateFromNow (date: string): string {
  return moment(date).fromNow();
}
