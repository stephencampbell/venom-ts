const path                = require('path');
const { VueLoaderPlugin } = require('vue-loader');

module.exports = {
  entry: './client/ts/app.ts',
  output: {
    path:     path.resolve(__dirname, 'dist'),
    filename: 'app.js'
  },
  module: {
    rules: [
      {
        test: /\.vue$/,
        loader: 'vue-loader'
      }, {
        test: /\.js$/,
        loader: 'babel-loader',
        exclude: file => (
          /node_modules/.test(file) &&
          !/\.vue\.js/.test(file)
        )
      }, {
        test: /\.css$/,
        use: [
          'vue-style-loader',
          'css-loader'
        ]
      }, {
        test: /\.s[ac]ss$/,
        use: [
          'vue-style-loader',
          'css-loader',
          'sass-loader'
        ]
      }, {
        test: /\.ts$/,
        loader: 'ts-loader',
        exclude: file => (
          /node_modules/.test(file)
        ),
        options: {
          transpileOnly: true,
          appendTsSuffixTo: [/.vue$/],
          configFile: 'tsconfig.webpack.json'
        }
      }
    ]
  },
  plugins: [
    new VueLoaderPlugin()
  ],
  resolve: {
    extensions: ['.js', '.ts', '.vue', '.css', '.sass', '.scss'],
    alias: {
      '~types': path.resolve(__dirname, 'shared/types/'),
      '~services': path.resolve(__dirname, 'client/ts/services/'),
      '~components': path.resolve(__dirname, 'client/ts/components/'),
      '~ts': path.resolve(__dirname, 'client/ts/'),
      '~shared': path.resolve(__dirname, 'shared/')
    }
  },
  devServer: {
    host: 'localhost',
    contentBase: path.join(__dirname, 'static'),
    compress: true,
    port: 9000,
    historyApiFallback: true,
    proxy: {
      '/api': 'http://localhost:3000'
    }
  }
};
