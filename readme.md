# Venom TS

This project is a template of a venom (Vue,
Express, Node and Mongo) stack that can be cloned
and used as a base for other projects.

Includes user registration and login authentication.
Sample pages are provided for demonstration and
reference.

Venom TS uses TypeScript for both back-end and front-end code.
If you want a Venom template without TypeScript, try [Venom auth](https://gitlab.com/stephencampbell/venom-auth).

[Docker](https://www.docker.com/) is required to run this project.

### Included
- [Vue](https://vuejs.org/)
- [Express](https://expressjs.com/)
- [Node](https://nodejs.org/en/)
- [Mongo](https://www.mongodb.com/)
- [TypeScript](https://www.typescriptlang.org/)
- Shared interfaces between front-end and back-end to enforce consistent typing
- [Mongoose](https://mongoosejs.com/) and templates
- Pre-configured Express routes
- User authentication
- [Socket.io](https://socket.io/docs/)
- [Sass](https://sass-lang.com/) support
- [Vue router](https://router.vuejs.org/)
- [Vuex](https://vuex.vuejs.org/)
- [Axios](https://github.com/axios/axios)
- [Webpack](https://webpack.js.org/)
- [Bootstrap](https://getbootstrap.com/)
- [FontAwesome](https://fontawesome.com/)

### Directory structure
- `/api` - uncompiled TypeScript server files
- `/build` - compiled Node-compatible JavaScript server files
- `/client` - uncompiled client files
- `/dist` - compiled client files, for distribution
- `/static` - files served to the client that don't need to be compiled
- `/config` - server-side configuration files
- `/shared` - files that are shared between client and server

### How to use
Start by cloning the project repository

`git clone https://gitlab.com/stephencampbell/venom-ts.git`

Next, navigate to the project directory

`cd venom-ts`

Install dependencies

`npm install`

Build backend assets

`npm run api:tsc`

Start docker-compose to run the server and Mongo docker containers

`docker-compose up`

Bundle frontend assets with Webpack

`npm run client:watch`

### Notes
- When making changes in `docker-compose.yml`
  or `Dockerfile`, run `docker-compose up --build`
  to see changes
- This project uses aliases to simplify imports from other parts
  of the file structure. Tilde (~) is used for client-side aliases
  and at (@) is used for server-side aliases.
