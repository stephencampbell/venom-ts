import { Document } from "mongoose";

interface IPost extends Document {
    title:     string
    content:   string
    slug:      string
    createdBy: string
    createdAt: Date
}

export default IPost;
