import { Document } from 'mongoose';

interface IMessage extends Document {
  message:   string
  createdBy: string,
  createdAt: Date
}

export default IMessage;
