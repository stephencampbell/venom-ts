export interface IUser {
  id:        string,
  firstName: string,
  lastName:  string,
  slug:      string
}

export default IUser;
