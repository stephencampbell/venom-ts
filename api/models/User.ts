import mongoose, { Schema, Document } from 'mongoose';

export interface IUser extends Document {
  firstName:    string
  lastName:     string
  emailAddress: string
  password:     string
  slug:         string
  createdAt:    Date
}

const userSchema = new Schema({
  firstName:    {
    type:     String,
    required: true
  },
  lastName:     {
    type:     String,
    required: true
  },
  emailAddress: {
    type:     String,
    required: true
  },
  password:     {
    type:     String,
    required: true
  },
  slug:         {
    type:     String,
    required: true
  },
  createdAt:    {
    type:    Date,
    default: Date.now
  }
});

export default mongoose.model<IUser>('users', userSchema);
