import mongoose, { Schema } from 'mongoose';
import IPost                from '@shared/types/post';

const postSchema = new Schema({
  title:     {
    type:     String,
    required: true
  },
  content:   {
    type:     String,
    required: true
  },
  slug:      {
    type:     String,
    required: true
  },
  createdBy: {
    type:     String,
    required: true
  },
  createdAt: {
    type:    Date,
    default: Date.now
  }
});

export default mongoose.model<IPost>('posts', postSchema);
