import mongoose, { Schema } from 'mongoose';
import IMessage             from '@shared/types/message';

const messageSchema: Schema = new Schema({
  message:   {
    type:     String,
    required: true
  },
  createdBy: {
    type:     String,
    required: true
  },
  createdAt: {
    type:    Date,
    default: Date.now
  }
});

export default mongoose.model<IMessage>('messages', messageSchema);
