// Node modules
import 'module-alias/register';
import morgan                                      from 'morgan';
import cors                                        from 'cors';
import passport                                    from 'passport';
import session                                     from 'express-session';
import path                                        from 'path';
import bodyParser                                  from 'body-parser';
import express, { Request, Response, Application } from 'express';
import mongoose, { Error }                         from 'mongoose';

// Keys
import keys from '@config/keys';

// Web socket server
import socket from './socket';

// Controller files
import { postRouter } from './controllers/posts';
import { userRouter } from './controllers/users';

// Passport
import authenticator from '@config/passport';

// Constants
const port           = 3000;
const mongoContainer = 'mongo';
const mongoDatabase  = 'venom';
const mongoPort      = 27017;
const host           = 'localhost';

// App
const app: Application = express();

// Database
const db = `mongodb://${mongoContainer}:${mongoPort}/${mongoDatabase}`; // Domain is docker container name
mongoose
  .connect(db, { useNewUrlParser: true })
  .then(() => console.log('MongoDB connected'))
  .catch((err: Error) => console.log(err));

app.use(session({
  secret:            keys.sessionKey,
  resave:            false,
  saveUninitialized: true,
  cookie:            { secure: false }
}));

// Morgan request logging
app.use(morgan('tiny'));

// CORS
app.use(cors());

// Body parser middleware
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

// Passport middleware
app.use(passport.initialize());

// Passport configuration
authenticator(passport);

// Use controllers
app.use('/api/posts', postRouter);
app.use('/api/users', userRouter);

// Static files
app.get('/', (req: Request, res: Response) => res.sendFile(path.join(__dirname, '/../static/index.html')));
app.use(express.static(path.join(__dirname, '/../dist')));
app.use(express.static(path.join(__dirname, '/../static')));

// Fallback route
app.all('*', (req: Request, res: Response) => res.sendFile(path.join(__dirname, '/../static/index.html')));

// Listen for requests
app.listen(port, () => console.log(`Listening on http://${host}:${port}`));

// Start web socket server
socket(host);
