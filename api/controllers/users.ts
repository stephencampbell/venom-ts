import { Request, Response, Router } from 'express';
import bcrypt                        from 'bcryptjs';
import jwt                           from 'jsonwebtoken';
import passport                      from 'passport';
import User, { IUser }               from '@models/User';
import slugify                       from 'slugify';
import { jwtKey }                    from '@config/keys';

const router = Router();

// @route  GET api/users/test
// @desc   Tests users route
// @access Public
router.get('/test', (req: Request, res: Response) => res.json({ msg: 'Users route works' }));

// @route  GET api/users/current
// @desc   Returns current user
// @access Private
router.get('/current', passport.authenticate('jwt', { session: false }), (req: Request, res: Response) => {
  return res.json(req.user);
});

// @route  GET api/users/all
// @desc   Gets all users
// @access Private
router.get('/all', passport.authenticate('jwt', { session: false }), (req: Request, res: Response) => {
  User
    .find({})
    .then((users: IUser[]) => res.json(users.map(u => ({
      id:        u.id,
      firstName: u.firstName,
      lastName:  u.lastName,
      slug:      u.slug
    }))));
});

// @route  GET api/users/:slug
// @desc   Gets a single user
// @access Private
router.get('/:slug', passport.authenticate('jwt', { session: false }), (req: Request, res: Response) => {
  User
    .findOne({ slug: req.params.slug })
    .then((u: IUser | null) => {
      if(u) {
        const user = {
          id:        u.id,
          firstName: u.firstName,
          lastName:  u.lastName,
          slug:      u.slug
        };
        res.json(user);
      }
    });
});

// @route  POST api/users/register
// @desc   Adds a new user
// @access Public
router.post('/register', (req: Request, res: Response) => {
  const firstName    = req.body.firstName.trim();
  const lastName     = req.body.lastName.trim();
  const slug         = slugify(`${firstName.toLowerCase()} ${lastName.toLowerCase()}`);
  const emailAddress = req.body.emailAddress.trim();

  User
    .findOne({ emailAddress })
    .then((user: IUser | null) => {
      if (user) {
        return res.status(400).json({ msg: 'Email address already registered' });
      } else {
        // Create mongoose user object
        const newUser = new User({
          firstName,
          lastName,
          emailAddress,
          password: req.body.password,
          slug
        });

        // Generate 10 character salt for password
        bcrypt.genSalt(10, (err, salt) => {
          if (err) throw err;

          // Hash password
          bcrypt.hash(newUser.password, salt, (err, hash) => {
            if (err) throw err;

            newUser.password = hash;

            // Save user
            newUser
              .save()
              .then((user: IUser) => res.json(user))
              .catch((err: Object) => console.log(err));
          });
        });
      }
    });
});

// @route  POST api/users/login
// @desc   Login user / return JWT token
// @access Public
router.post('/login', (req, res) => {
  const emailAddress = req.body.emailAddress.trim();
  const password     = req.body.password;

  // Find user by email address
  User
    .findOne({ emailAddress })
    .then((user: IUser | null) => {
      if (user) {
        // Compare plain text password to hashed password
        bcrypt.compare(password, user.password)
              .then(isMatch => {
                if (isMatch) {
                  // Store login in the session
                  const ssn = req.session;
                  if(ssn)
                    ssn.user  = user;

                  // Create JWT payload
                  const payload = {
                    id:        user.id,
                    firstName: user.firstName,
                    lastName:  user.lastName
                  };

                  // Sign token
                  jwt.sign(payload, jwtKey, { expiresIn: 3600 }, (err, token) => {
                    // Send token as response
                    res.json({
                      token: `Bearer ${token}`,
                      user:  {
                        id:           user.id,
                        firstName:    user.firstName,
                        lastName:     user.lastName,
                        emailAddress: user.emailAddress,
                        slug:         user.slug
                      }
                    });
                  });
                } else {
                  return res.status(400).json({ msg: 'Incorrect password' });
                }
              })
      } else {
        return res.status(404).json({ msg: 'Email address not registered' });
      }
    });
});

export { router as userRouter };
