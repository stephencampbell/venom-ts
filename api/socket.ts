// Node modules
import express from 'express';
import http    from 'http';
import io      from 'socket.io';

// Interfaces
interface IUser {
  userName: string
}

interface IMessageRequestData {
  userId:  string
  message: string
}

// Message model
import Message  from '@models/Message';
import IMessage from '@shared/types/message';

const webSocketPort = 3001;

// Set up server
const webSocket = express();
const server    = new http.Server(webSocket);

let sio: io.Server = io(server);

// List of active user names
let users: IUser[] = [];

// Web sockets
sio.on('connection', (socket: any, data: any) => {
  socket.userName = socket.handshake.query.userName;

  users.push({
    userName: socket.userName
  });

  sio.sockets.emit('users-updated', { users });

  // On connection, load recent messages
  Message
    .find({})
    .sort({ 'createdAt': -1 })
    .limit(10)
    .then((messages: IMessage[]) => {
      socket.emit('messages-loaded', messages)
    });

  // Receive sent messages from the client
  socket.on('send-message', function (data: IMessageRequestData) {
    const userId  = data.userId;
    const message = data.message;

    const newMessage = new Message({
      createdBy: userId,
      message:   message
    });

    newMessage
      .save()
      .then(message => {
        // Broadcast new message
        sio.sockets.emit('message-saved', message)
      })
      .catch(err => console.log(err));
  });

  socket.on('disconnect', function (data: any) {
    users = users.filter(u => u.userName !== socket.userName);
    sio.sockets.emit('users-updated', { users });
  });
});

export default (host: string) => {
  // Web socket server
  server.listen(webSocketPort, () => console.log(`Web socket server listening on http://${host}:${webSocketPort}`));
};
