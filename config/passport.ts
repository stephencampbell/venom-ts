import { Strategy, ExtractJwt } from 'passport-jwt';
import { jwtKey }               from './keys';
import mongoose                 from 'mongoose';
import { PassportStatic }       from 'passport';

const User = mongoose.model('users');

const opts = {
  jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
  secretOrKey:    jwtKey
};

export default (passport: PassportStatic) => {
  passport.use(new Strategy(opts, (jwtPayload, done) => {
    User
      .findById(jwtPayload.id)
      .then(user => {
        if (user) {
          return done(null, user)
        } else {
          return done(null, false)
        }
      });
  }));
};
